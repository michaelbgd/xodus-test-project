package com.dummy.xodus;

import jetbrains.exodus.entitystore.EntityIterable;
import jetbrains.exodus.entitystore.PersistentEntityStoreImpl;
import jetbrains.exodus.entitystore.PersistentEntityStores;

public class Runner {

    private static String TYPE = "type";

    public static void main(String[] args) {
        final PersistentEntityStoreImpl store = PersistentEntityStores.newInstance("xodusdata");
        System.out.println("read " + storeAndCount(store) + " entities");
        System.out.println("read " + storeAndCount(store) + " entities");
        store.close();
    }

    private static long storeAndCount(PersistentEntityStoreImpl store) {
        store.executeInTransaction(txn -> {
            for (int i = 0; i < 2000; i++) {
                txn.newEntity(TYPE).setProperty("someProperty", "someValue" + i);
            }
        });

        return store.computeInReadonlyTransaction(txn -> {
            final EntityIterable all = txn.getAll(TYPE);
            all.forEach(entity -> entity.getProperty("someProperty"));
            return all.count();
        });
    }
}
